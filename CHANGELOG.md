# Changelog

## [1.1.1] 2023-07-18

### Fixed

- Rendre la table BREVES conditionnelle pour SPIP 4+

### Changed

- Ajout d'un CHANGELOG.md et README.md

