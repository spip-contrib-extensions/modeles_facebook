<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/fbmodeles?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cf_navigation' => 'Vedi la [colonna di navigazione->@url@]',
	'cfg_comment_appid' => 'Identificativo di un’applicazione specifica per il tuo sito; ciò richiede di aver prima creato l’applicazione.',
	'cfg_comment_border_color' => 'Indica un colore in codice esadecimale CON l’hash iniziale.',
	'cfg_comment_colorscheme' => 'Seleziona qui il profilo predeterminato dai moduli che verranno utilizzati per la visualizzazione.',
	'cfg_comment_font' => 'Seleziona qui il font che verrà utilizzato per la visualizzazione dei moduli.',
	'cfg_comment_identifiants' => '{{Utilizza i campi sottostanti per specificare i diversi identificatori che desideri utilizzare.}} Non sono obbligatori, ma possono essere utilizzati in particolare per monitorare statistiche specifiche offerte da Facebook.',
	'cfg_comment_pageid' => 'Identificativo di una pagina Facebook; ciò richiede di aver prima creato la pagina.',
	'cfg_comment_reglages' => '{{Qui puoi scegliere alcune impostazioni relative agli strumenti javascript di Facebook.}} Per impostazione predefinita, i modelli utilizzano il linguaggio XFBML ({SDK javascript Facebook}) ma puoi disattivare questa funzione; gli strumenti verranno quindi caricati in frame.',
	'cfg_comment_url_page' => 'Indirizzo completo della tua pagina o profilo Facebook; verrà utilizzato per impostazione predefinita dai modelli  (URL del tipo "<code>http://www.facebook.com/...</code>").',
	'cfg_comment_userid' => 'ID utente dell’amministratore dei plugin. Puoi indicarne più di uno separandoli con una virgola.',
	'cfg_comment_xfbml' => 'Utilizzo della libreria javascript dell’SDK di Facebook e della lingua associata. Se scegli "NO", i moduli verranno presentati in iframe.',
	'cfg_descr' => 'Qui è necessario indicare i diversi identificativi forniti da Facebook.<br /><br />Per ulteriori informazioni: [->http://www.facebook.com/insights/].

Per includere i tag "Open Graph" nell’intestazione delle tue pagine pubbliche, devi inserire il template "insert_head_og" passandogli l’ambiente: <code>#MODELE{insert_head_og}{env}</code>.
<br /><br />Per ulteriori informazioni: [->http://developers.facebook.com/docs/opengraph/].',
	'cfg_descr_titre' => 'Facebook Models',
	'cfg_identifiants' => 'Identificativi di Facebook',
	'cfg_label_appid' => 'Identificativo dell’applicazione "ID app"',
	'cfg_label_border_color' => 'Colore predefinito del bordo',
	'cfg_label_colorscheme' => 'Profilo colore',
	'cfg_label_font' => 'Font predefinito utilizzato',
	'cfg_label_pageid' => 'Identificativo pagina "Page ID"',
	'cfg_label_titre' => 'Configurazione di Facebook Models',
	'cfg_label_url_page' => 'URL della pagina o del profilo',
	'cfg_label_userid' => 'Identificativo utente "User ID"',
	'cfg_label_xfbml' => 'Utilizzo di XFBML',
	'cfg_reglages' => 'Impostazioni predefinite',

	// D
	'defaut' => 'Default',
	'doc_chapo' => 'Il plugin Facebook Models per SPIP 2.0 ({ed oltre}) offre una serie di modelli per utilizzare in modo rapido e semplice i plug-in social offerti da Facebook.',
	'doc_en_ligne' => 'Documentazione',
	'doc_titre_court' => 'Documentazione Facebook Models',
	'doc_titre_page' => 'Pagina della documentazione del plugin Facebook Models',
	'documentation' => '{{{Utilizzo del plugin}}}

Come mostrato sopra, i modelli sono inclusi direttamente passando loro le opzioni desiderate.

Ad ogni modello può essere fornito un elenco di opzioni, alcune delle quali necessarie per la sua visualizzazione. Per un elenco completo, vedere le informazioni sull’intestazione del file del modello nella directory "<code>modeles/</code>" del plugin.

Il plugin offre anche un modello che genera informazioni  {{Open Graph}}, le meta informazioni utilizzate da Facebook, specifiche per ogni oggetto SPIP. Per usarlo, devi aggiungere manualmente il modello "{{insert_head_og}}".

{{Attenzione!}} Questo modello richiede la ricezione dell’ambiente corrente, quindi è necessario includerlo in ciascuno dei modelli di pagina ({"article.html", "rubrique.html" ...}) e non nell’intestazione globale inclusione ({"inc_head.html"}) che indica: 
<cadre class=\'spip\'>
{{#MODELE{insert_head_og}{env}}}
</cadre>',

	// E
	'exemple' => '{{{Esempio}}}

I diversi blocchi di seguito mostrano un esempio di ciascun modello con valori fittizi. Fare riferimento al modello corrispondente per le opzioni.',

	// F
	'fb_modeles' => 'Facebook Models',

	// I
	'info_doc' => 'Se hai problemi a visualizzare questa pagina, [clicca qui->@link@].',
	'info_doc_titre' => 'Nota sulla visualizzazione di questa pagina',
	'info_skel_contrib' => 'La pagina con la documentazione completa si trova suspip-contrib: [->https://contrib.spip.net/?article3872].',
	'info_skel_doc' => 'Questa pagina di documentazione è progettata come un modello SPIP che opera con la distribuzione standard ({file nella directory "squelettes-dist/"}).  Se non riesci a visualizzare la pagina, oppure il tuo sito utilizza i propri template, i link sottostanti ti consentono di gestirne la visualizzazione:

-* [Modalità "testo semplice"->@mode_brut@] ({html semplice + tag INSERT_HEAD})
-* [Modalità "template Zpip"->@mode_zpip@] ({template Z compatibile})
-* [Modalità "template SPIP"->@mode_spip@] ({distribuzione compatibile})',

	// J
	'javascript_inactif' => 'JavaScript non è attivo sul tuo browser; alcune funzionalità di questo strumento saranno inattive...',

	// L
	'licence' => 'Plugin per SPIP 2.0+ : {{"Facebook Models" - copyright © 2009 [Piero Wbmstr->https://contrib.spip.net/PieroWbmstr] con licenza [GPL->http://www.opensource.org/licenses/gpl-3.0.html] }}.',

	// N
	'new_window' => 'Nuova finestra',
	'non' => 'No',

	// O
	'oui' => 'Si',

	// P
	'page_test' => 'Pagina di test (locale)',
	'page_test_in_new_window' => 'Pagina di test in una nuova finestra',
	'personnalisation' => '{{{Personalizzazione}}}

Ogni modello presenta il suo contenuto in un blocco di tipo  <code>div</code> che trasporta classi CSS di tipo <code>fb_modeles fb_XXX</code> dove {{XXX}} è il nome del modello. Ciò consente la personalizzazione degli stili per tutti i modelli e per ciascuno di essi.


Ad esempio per il modulo "Invia" di Facebook:
<cadre class="spip">
<div class="fb_modeles fb_send">
     ... contenuti ... 
</div>
</cadre>',

	// S
	'sep' => '----',

	// T
	'titre_original' => 'Facebook Models, plugin per SPIP 2.0+'
);
