<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-fb_modeles?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'fb_modeles_description' => 'Questo plugin offre una serie di template SPIP per integrare facilmente e velocemente i moduli social di Facebook:

-*  {{send}} : il classico pulsante « Invia » (con commento)
-*  {{like}} : il classico pulsante « Mi piace » (con commento)
-*  {{like box}} : elenco dei commenti degli utenti che hanno messo « Mi piace »
-*  {{activity}} : Lista delle attività recenti su una pagina o profilo
-*  {{comments}} : elenco dei commenti e pulsante per scriverne uno
-*  {{live stream}} (sperimentale) : chat box',
	'fb_modeles_nom' => 'Facebook Models',
	'fb_modeles_slogan' => 'Modelli per includere i social plugin di Facebook'
);
